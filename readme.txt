

Installation
------------

```bash
pip3 install libevdev python-socketio eventlet
```


Run
---

```bash
sudo ./main.py
```


Links
-----

https://gitlab.freedesktop.org/libevdev/python-libevdev