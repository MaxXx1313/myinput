#!/usr/bin/env python3

import sys
import time
import math
import threading
import asyncio

import libevdev
from libevdev import InputEvent
from libevdev import EventCode

import eventlet
import socketio


AXIS_MAX = 16534;

WS_HOST = "" # all hosts
WS_PORT = 12344

# a [-90; 90]
# b [-180; 180]
# y [0; 360]
orientation = {
    'a': 0,
    'b': 0,
    'y': 0
}
buttons = {
    'a': 0,
    'b': 0,
    'x': 0,
    'y': 0
}

## WEBSOCKET
def wsConnection():
    # start_server = websockets.serve(processRequest, WS_HOST, WS_PORT)
    # print("Websocket at {} ({})".format(WS_HOST, WS_PORT))
    # create a Socket.IO server
    sio = socketio.Server()
    app = socketio.WSGIApp(sio, static_files={
        '/': {'content_type': 'text/html', 'filename': 'public/index.html'},
        '/gyronorm.min.js': {'content_type': 'text/javascript', 'filename': 'public/gyronorm.complete.min.js'},
        '/socket.io.js': {'content_type': 'text/javascript', 'filename': 'public/socket.io.js'},
        '/socket.io.js.map': {'filename': 'public/socket.io.js.map'}, # no such file, we just eliminate the error in webserver
        '/favicon.ico': {'content_type': 'text/html', 'filename': 'public/favicon.ico'}
    })

    @sio.event
    def test(sid, data):
        # handle the message
        greeting = f"Hello {data}!"
        print(f"> {greeting}")
        sio.emit("test", greeting, room=sid)

    @sio.event
    def ori(sid, data):
        # handle the message
        # greeting = f"Ori {data}!"
        # print(f"> {greeting}")
        global orientation;
        orientation['a'] = int(data['a'] * AXIS_MAX / 180)
        orientation['b'] = int(-1 * data['b'] * AXIS_MAX / 180)
        orientation['y'] = int(data['y'] * AXIS_MAX / 360)
        sio.emit("ori", data['t'], room=sid)


    @sio.event
    def btns(sid, data):
        global buttons;
        buttons['a'] = data['a']
        buttons['b'] = data['b']
        buttons['x'] = data['x']
        buttons['y'] = data['y']

    @sio.event
    def connect(sid, environ):
        print('connect ', sid)


    @sio.event
    def disconnect(sid):
        print('disconnect ', sid)


    print("Starting server at {}:{}".format(WS_HOST, WS_PORT))
    eventlet.wsgi.server(eventlet.listen((WS_HOST, WS_PORT)), app)


## PAD

# class _InputAbsinfo(ctypes.Structure):
#     _fields_ = [("value", c_int32),
#                 ("minimum", c_int32),
#                 ("maximum", c_int32),
#                 ("fuzz", c_int32),
#                 ("flat", c_int32),
#                 ("resolution", c_int32)]

def virtualMouse():
    dev = libevdev.Device()

    dev.name = "MySampleMouse"
    dev.enable(libevdev.EV_REL.REL_X)
    dev.enable(libevdev.EV_REL.REL_Y)
    dev.enable(libevdev.EV_KEY.BTN_LEFT)
    dev.enable(libevdev.EV_KEY.BTN_RIGHT)


    x0 = None;
    y0 = None;
    BTN_LEFT = 0;
    SPEEDX = -0.2; # * math.floor( 1920/180 );
    SPEEDY = 0.1; # * math.floor( 1080/90 );

    try:
        uinput = dev.create_uinput_device()
        print("New pointer device at {} ({})".format(uinput.devnode, uinput.syspath))

        # Sleep for a bit so udev, libinput, Xorg, Wayland, ... all have had
        # a chance to see the device and initialize it. Otherwise the event
        # will be sent by the kernel but nothing is ready to listen to the
        # device yet.
        time.sleep(1)
        

        while True:
            # events = [];

            if x0 is None:
                x0 = orientation['y'];
            if y0 is None:
                y0 = orientation['b'];

            moveX = math.floor( (orientation['y'] - x0) * SPEEDX );
            x0 = orientation['y'];

            moveY = math.floor( (orientation['b'] - y0) * SPEEDY );
            y0 = orientation['b'];
            
            # events.append(InputEvent(libevdev.EV_REL.REL_X, moveX))
         
            events = [
                InputEvent(libevdev.EV_REL.REL_X, moveX),
                InputEvent(libevdev.EV_REL.REL_Y, moveY),
            ]

            if (BTN_LEFT != buttons['a'] ):
                BTN_LEFT = buttons['a']
                events.append(InputEvent(libevdev.EV_KEY.BTN_LEFT, BTN_LEFT))

            # if(orientationPrev['y'] != orientation['y']):
            #     orientationPrev['y'] = orientation['y']
            #     events.append(InputEvent(libevdev.EV_ABS.ABS_Z, orientation['b']))

            if (len(events) > 0):
                events.append(InputEvent(libevdev.EV_SYN.SYN_REPORT, 0))
                time.sleep(0.012)
                uinput.send_events(events)
            else:
                time.sleep(0.012)

    except OSError as e:
        print(e)


## MAIN
def main(args):
    t1 = threading.Thread(target=virtualMouse)
    # t2 = threading.Thread(target=wsConnection)

    # start threads
    t1.start()
    # t2.start()

    # wait until threads finish their job
    # t1.join()
    # t2.join()

    # there is a trouble running websocket in a separate thread, so we run it in main thread =)
    wsConnection()

## CLI
if __name__ == "__main__":
    main(sys.argv)
